��            )   �      �  ;   �     �     �     �            `   >  )   �  E   �  <     >   L  <   �  0   �     �          *     1     J     S     f  *   m  O   �     �     �  	   �            (         G  �  h  J        c     }  -   �     �  #   �  �   �  5   r	  ]   �	  U   
  ;   \
  8   �
  8   �
  "   
      -     N  )   T     ~     �     �  <   �  n   �     P     V     \     e     {  5   �  /   �                              	                                                                                  
                         0 to deactivate sending of email, empty to use default (%s) All week days At all moment Day of week for sending email Friday In days, empty for default (%s) Leave empty to send all available emails,only limited by global batch size (%s) for all surveys. Leave empty to send all available emails. Max email to send (invitation + remind) in one batch for this survey. Max email to send (invitation + remind) to each participant. Max email to send for invitation in one batch for this survey. Max email to send for reminder in one batch for this survey. Min delay between invitation and first reminder. Min delay between reminders. Moment for emailing Monday Only if no moment is set Saturday Send reminders to  Sunday The max email setting can not be exceeded. The max email setting can not be exceeded. Reminders are sent after invitation. Thuesday Thursday Wednesday all participants disabled participants who did not started survey. participants who started survey. Project-Id-Version: sendMailCron
POT-Creation-Date: 2017-03-16 15:03+0100
PO-Revision-Date: 2017-03-16 15:04+0100
Last-Translator: 
Language-Team: 
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.12
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: _translate
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
 0 pour désactiver l'envoi de messages, vide pour utiliser le défaut (%s) Tous les jours de semaine À tous les moments Jours de la semaine pour envoyer les messages Vendredi En jours, vide pour le défaut (%s) Laisser vide pour envoyer tous les messages possibles, seulement limité par la taille du lot global (%s) de tous les questionnaires/ Laisser vide pour envoyer tous les messages possibles Nombre maximum de messages à envoyer (invitations et rappels) par lot pour ce questionnaire. Nombre de messages maximum à envoyer (invitation et rappel(s)) à chaque participant Nombre maximum d'invitations par lot pour ce questionnaire. Nombre maximum de rappels par lot pour ce questionnaire. Délais minimum entre l'invitation et le premier rappel. Délais minimums entre les rappels Moment pour l'envoi des messages Lundi Seulement si le moment n'est pas indiqué Samedi Envoi des relance à  Dimanche Le nombre maximum de messages par lot ne sera pas dépassé. Le nombre maximum de messages par lot ne sera pas dépassé. Les rappels sont envoyés après les invitations. Jeudi Mardi Mercredi tous les participants désactivé participants qui n'ont pas commencé le questionnaire participants qui ont commencé le questionnaire 